const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
const ipcMain = electron.ipcMain
const clipboard = electron.clipboard
const Notification = electron.Notification
const Menu = electron.Menu

const path = require('path')
const url = require('url')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
let passwordWindow = null
let addPasswordWindow = null
let editPasswordWindow = null
let viewPasswordWindow = null
let changeMasterPasswordWindow = null

function createWindow () {
  
  let { width, height } = store.get('windowBounds');

  // Create the browser window.
  mainWindow = new BrowserWindow({ width, height })

  mainWindow.on('resize', () => {
    // The event doesn't pass us the window size, so we call the `getBounds` method which returns an object with
    // the height, width, and x and y coordinates.
    let { width, height } = mainWindow.getBounds();
    // Now that we have them, save them using the `set` method.
    store.set('windowBounds', { width, height });
  });

  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })

  ipcMain.on('cancel', (event, arg) => {
    
    if (passwordWindow      != null) passwordWindow.close()
    if (addPasswordWindow   != null) addPasswordWindow.close()
    if (editPasswordWindow  != null) editPasswordWindow.close()
    if (viewPasswordWindow  != null) viewPasswordWindow.close()
    if (changeMasterPasswordWindow  != null) changeMasterPasswordWindow.close()
  })

  // Create the Application's main menu
  var template = [{
    label: "SSPass",
    submenu: [
      { label: "About SSPass", selector: "orderFrontStandardAboutPanel:" },
      { type: "separator" },
      { label: "Quit SSPass", accelerator: "Command+Q", click: function() { app.quit(); }}
    ]}, {
    label: "Edit",
    submenu: [
      { label: "Undo", accelerator: "CmdOrCtrl+Z", selector: "undo:" },
      { label: "Redo", accelerator: "Shift+CmdOrCtrl+Z", selector: "redo:" },
      { type: "separator" },
      { label: "Cut", accelerator: "CmdOrCtrl+X", selector: "cut:" },
      { label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:" },
      { label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:" },
      { label: "Select All", accelerator: "CmdOrCtrl+A", selector: "selectAll:" }
    ]}
  ];

  Menu.setApplicationMenu(Menu.buildFromTemplate(template));
}

function createPasswordWindow(callback) {

  passwordWindow = new BrowserWindow({
    parent: mainWindow,
    modal: true,
    width: 300, 
    height: 150
  })

  passwordWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'password.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Emitted when the window is closed.
  passwordWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    passwordWindow = null

    ipcMain.removeAllListeners('master_password')
  })

  ipcMain.once('master_password', (event, arg) => {
    
    callback(arg)
    passwordWindow.close()
  })
}

function createAddPasswordWindow(callback) {

  addPasswordWindow = new BrowserWindow({
    parent: mainWindow,
    modal: true,
    width: 400,
    height: 340
  })

  addPasswordWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'addPassword.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Emitted when the window is closed.
  addPasswordWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    addPasswordWindow = null

    ipcMain.removeAllListeners('add_password')
  })

  ipcMain.once('add_password', (event, arg) => {
    
    callback(arg)
    addPasswordWindow.close()
  })
}

function createChangeMasterPasswordWindow(callback) {

  changeMasterPasswordWindow = new BrowserWindow({
    parent: mainWindow,
    modal: true,
    width: 400,
    height: 340
  })

  changeMasterPasswordWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'changeMasterPassword.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Emitted when the window is closed.
  changeMasterPasswordWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    changeMasterPasswordWindow = null

    ipcMain.removeAllListeners('change_master_password')
  })

  ipcMain.once('change_master_password', (event, arg) => {
    
    callback(arg)
    changeMasterPasswordWindow.close()
  })
}

function createEditPasswordWindow(id, description, username, password, callback) {

  ipcMain.removeAllListeners('get_password_data_req')
  ipcMain.on('get_password_data_req', (event, arg) => {

    event.sender.send('get_password_data_resp', {
      
      id: id,
      description: description,
      username: username,
      password: password
    })
  })
  
  editPasswordWindow = new BrowserWindow({
    parent: mainWindow,
    modal: true,
    width: 400, 
    height: 340
  })

  editPasswordWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'editPassword.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Emitted when the window is closed.
  editPasswordWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    editPasswordWindow = null

    ipcMain.removeAllListeners('edit_password')
  })

  ipcMain.once('edit_password', (event, arg) => {
    
    callback(arg)
    editPasswordWindow.close()
  })
}

function createViewPasswordWindow(description, username, password) {
  
  ipcMain.removeAllListeners('get_password_data_req')
  ipcMain.on('get_password_data_req', (event, arg) => {

    event.sender.send('get_password_data_resp', {
      
      description: description,
      username: username,
      password: password
    })
  })

  viewPasswordWindow = new BrowserWindow({
    parent: mainWindow,
    modal: true,
    width: 400, 
    height: 340
  })

  viewPasswordWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'viewPassword.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Emitted when the window is closed.
  viewPasswordWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    viewPasswordWindow = null
  })
}
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

const dialog = electron.dialog
exports.selectDirectory = function (callback) {
  dialog.showOpenDialog(
    mainWindow,
    {
      properties: ['openDirectory']
    },
    callback
  )
}

const Store = require('./store.js')
const store = new Store({
  // We'll call our data file 'user-preferences'
  configName: 'prefs',
  defaults: {
    // 800x600 is the default size of our window
    windowBounds: { width: 800, height: 600 }
  }
})

exports.store = store

const SecureStore = require('./secure-store.js')
const secureStore = new SecureStore()
exports.secureStore = secureStore

exports.askForPassword = function(callback) {
  
  //TODO: Validazione campi
  createPasswordWindow(callback)
}

exports.viewPassword = function(description, username, password) {

  //TODO: Validazione campi
  createViewPasswordWindow(description, username, password)
}

exports.editPassword = function(id, description, username, password, callback) {

  //TODO: Validazione campi
  createEditPasswordWindow(id, description, username, password, callback)
}

exports.addPassword = function(callback) {
  
  //TODO: Validazione campi
  createAddPasswordWindow(callback)
}

exports.changeMasterPassword = function(callback) {

  //TODO: Validazione campi
  createChangeMasterPasswordWindow(callback)
}

exports.message = function(type, title, message, callback) {

  dialog.showMessageBox(
    mainWindow, 
    {
      type:type,
      title:title,
      message:message
    },
    callback
  )
}

exports.writeToClipboard = function(text) {

  clipboard.writeText(text)
}