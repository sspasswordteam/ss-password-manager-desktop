// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const electron = require('electron')
const remote = electron.remote
const mainProcess = remote.require('./main')

$.expr[":"].contains = $.expr.createPseudo(function(arg) {
    return function( elem ) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

function selectPasswordDatabase(pdb) {

  	if (mainProcess.secureStore.setPath(pdb)) {

		$('#passwordDatabase').val(pdb)
  		readPDB()
  	} else {

		mainProcess.message("info", "Information", "Password database does not exist, please insert a new Master Password for the new database", function() {

			mainProcess.askForPassword(function(mpassword) {

				var error = mainProcess.secureStore.createStore(pdb, mpassword)

				if (error == null) {

					$('#passwordDatabase').val(pdb)
					$('#openPasswordDatabase_btn').hide()
					$('#newPassword_btn').show()
					$('#password_table').show()
					$('#updateMasterPassword_btn').show()
					var data = {};
					renderPassword(data)
				} else {

					console.log(error)
					mainProcess.message("error", "Error", "Error during password database creation: " + error, null)
				}
			})
		})
  	}
}

function selectPasswordDatabasePath() {

  	mainProcess.selectDirectory(function(filePaths) {
	  	
	  	if (filePaths && filePaths.length > 0) {

	  		var pdb = filePaths[0]

		  	mainProcess.store.set('passwordDB', pdb)
		  	
		  	selectPasswordDatabase(pdb)
	  	}
	})
}

$('#passwordDatabase_btn').click(_ => {
  
  	selectPasswordDatabasePath()
})

$('#updateMasterPassword_btn').click(_ => {

	mainProcess.changeMasterPassword(function(args) {

		var mpassword = args.password
		var new_mpassword = args.n_password

		try {

			mainProcess.secureStore.changePassword(new_mpassword, mpassword)

			new Notification("SS Password Manager", {title:"SS Password Manager",body:"Master Password Updated"})
		} catch (error) {

			console.log(error)
			mainProcess.message("error", "Error", "Invalid Master Password", null)
		}
	})
})

$('#inputSearchPass').keyup(_ => {

	var searchText = $('#inputSearchPass').val();

	if (searchText == "") {

		$("#passwordList .passwordDescription").closest("tr").show();
	} else {

		$("#passwordList .passwordDescription").closest("tr").hide();
		$("#passwordList .passwordDescription:contains('" + $('#inputSearchPass').val() + "')").closest('tr').show();
	}
})

var pdb = mainProcess.store.get('passwordDB')
if (pdb != undefined) {

	selectPasswordDatabase(pdb)
} else {

	selectPasswordDatabasePath()
}

function renderPassword(data) {

	$('#passwordList').html('') 

	for (var id in data) {

		var elem = data[id]
		$('#passwordList').append('<tr><td class="passwordDescription">' + elem.description + '</td><td><p><button type="button" data-id="' + id + '" class="btn btn-primary pwd_view"><i class="glyphicon glyphicon-eye-open"></i></button><button style="margin-left:10px;" type="button" data-id="' + id + '" class="btn btn-primary pwd_copy"><i class="glyphicon glyphicon-paperclip"></i></button><button style="margin-left:10px;" type="button" data-id="' + id + '" class="btn pwd_edit"><i class="glyphicon glyphicon-pencil"></i></button><button style="margin-left:10px;" type="button" data-id="' + id + '" class="btn btn-danger pwd_delete"><i class="glyphicon glyphicon-trash"></i></button></p></td></tr>');
	}

	$(".pwd_view").click(function() {
		
		var id = $(this).data('id')

		mainProcess.askForPassword(function(mpassword) {

			try {

				var data = mainProcess.secureStore.get(id, mpassword)

				mainProcess.viewPassword(data.description, data.username, data.password)
			} catch (error) {

				console.log(error)
				mainProcess.message("error", "Error", "Invalid Master Password", null)
			}
		})
	})

	$(".pwd_copy").click(function() {

		var id = $(this).data('id')

		mainProcess.askForPassword(function(mpassword) {

			try {

				var data = mainProcess.secureStore.get(id, mpassword)

				mainProcess.writeToClipboard(data.password)

				new Notification("SS Password Manager", {title:"SS Password Manager",body:"Password copied in clipboard"})
			} catch (error) {

				console.log(error)
				mainProcess.message("error", "Error", "Invalid Master Password", null)
			}
		})
	})

	$(".pwd_edit").click(function() {
		
		var id = $(this).data('id')

		mainProcess.askForPassword(function(mpassword) {

			try {

				var data = mainProcess.secureStore.get(id, mpassword)

				mainProcess.editPassword(id, data.description, data.username, data.password, function(elem) {

					try {

						var data = mainProcess.secureStore.update(
							elem.id,
							elem.description,
							elem.username,
							elem.password,
							mpassword
						)

						renderPassword(data)
					} catch (error) {

						console.log(error)
						mainProcess.message("error", "Error", "Invalid Master Password", null)
					}
				})
			} catch (error) {

				console.log(error)
				mainProcess.message("error", "Error", "Invalid Master Password", null)
			}
		})
	})

	$(".pwd_delete").click(function() {

		var id = $(this).data('id')

		mainProcess.askForPassword(function(mpassword) {

			try {

				var data = mainProcess.secureStore.remove(id, mpassword)

				//TODO: Confirm
				renderPassword(data)
			} catch (error) {

				console.log(error)
				mainProcess.message("error", "Error", "Invalid Master Password", null)
			}
		})
	})
}

function readPDB() {

	mainProcess.askForPassword(function(password) {

		try {

			$('#openPasswordDatabase_btn').hide()
			$('#newPassword_btn').show()
			$('#password_table').show()
			$('#inputSearchPass').show()
			$('#updateMasterPassword_btn').show()
			var data = mainProcess.secureStore.loadAll(password);
			renderPassword(data)
		} catch (error) {

			console.log(error)
			mainProcess.message("error", "Error", "Invalid Master Password", null)
			$('#openPasswordDatabase_btn').show()
			$('#newPassword_btn').hide()
			$('#password_table').hide()
			$('#inputSearchPass').hide()
			$('#updateMasterPassword_btn').hide()
			renderPassword({})
		}
	})
}

$('#openPasswordDatabase_btn').click(_ => {

	readPDB()
})

$('#newPassword_btn').click(_ => {

	mainProcess.addPassword(function(elem) {

		mainProcess.askForPassword(function(mpassword) {

			try {

				var data = mainProcess.secureStore.add(
					elem.description,
					elem.username,
					elem.password,
					mpassword
				)

				renderPassword(data)
			} catch (error) {

				console.log(error)
				mainProcess.message("error", "Error", "Invalid Master Password", null)
			}
		})
	})
})