const electron = require('electron')
const crypto = require('crypto')
const path = require('path')
const fs = require('fs')

class SecureStore {

  constructor() {
	
	  this.algorithm = 'aes-256-ctr'
  }

  createStore(pdbDir, mpassword) {

    var p = path.join(pdbDir, 'index.dat')

    if (!fs.existsSync(p)) {

      try {

        this.path = p
        this.encryptAndSave("{}", mpassword)
        return null
      } catch(error) {

        return error
      }
    } else {

      return "File already exists"
    }
  }

  setPath(pdbDir) {

    var p = path.join(pdbDir, 'index.dat')

    if (fs.existsSync(p)) {

      this.path = p
      return true;
    } else {

      return false;
    }
  }

  loadAll(mpassword) {

    try {

      return JSON.parse(this.loadAndDecrypt(mpassword))
    } catch(error) {

      throw "Invalid password"
    }
  }

  get(id, mpassword) {

  	var data = this.loadAll(mpassword)

    return data[id]
  }

  remove(id, mpassword) {

    var data = this.loadAll(mpassword)

    delete data[id]

    this.encryptAndSave(JSON.stringify(data), mpassword)

    return data
  }

  add(description, username, password, mpassword) {

    var data = this.loadAll(mpassword)

    var id = 0

    while (data[id] != undefined) {

      id++
    }

    data[id] = {

      "description":description,
      "username":username,
      "password":password
    }

    this.encryptAndSave(JSON.stringify(data), mpassword)
    
    return data
  }
  
  update(id, description, username, password, mpassword) {

  	var data = this.loadAll(mpassword)

    data[id] = {
      
      "description":description,
    	"username":username,
    	"password":password
    }

    this.encryptAndSave(JSON.stringify(data), mpassword)

    return data
  }

  changePassword(new_mpassword, mpassword) {

    var data = this.loadAll(mpassword)

    this.encryptAndSave(JSON.stringify(data), new_mpassword)
  }

  //Security
  loadAndDecrypt(password) {

  	var buffer = fs.readFileSync(this.path)
  	var decipher = crypto.createDecipher(this.algorithm, password)
    var decrypted = Buffer.concat([decipher.update(buffer) , decipher.final()])
    return decrypted
  }

  encryptAndSave(data, password) {

  	var buffer = new Buffer(data, "utf8")
  	var cipher = crypto.createCipher(this.algorithm, password)
	  var crypted = Buffer.concat([cipher.update(buffer), cipher.final()])
    fs.writeFileSync(this.path, crypted)
  }
}

// expose the class
module.exports = SecureStore