// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const electron = require('electron')
const remote = electron.remote
const mainProcess = remote.require('./main')

const ipcRenderer = require('electron').ipcRenderer;

ipcRenderer.on('get_password_data_resp', (sender, data) => {

	$('#description').val(data.description)
	$('#username').val(data.username)
	$('#password').val(data.password)
})

$('#cancel_btn').click(_ => {

	ipcRenderer.sendSync('cancel', null)
})

$('#togglePassword_btn').click(_ => {

	if ($('#password').prop('type') == 'text') {

		$('#password').prop('type', 'password')
		$('#togglePassword_btn').html('<i class="glyphicon glyphicon-eye-open"></i>')
	} else {

		$('#password').prop('type', 'text')
		$('#togglePassword_btn').html('<i class="glyphicon glyphicon-eye-close"></i>')
	}
})

ipcRenderer.send('get_password_data_req', null)