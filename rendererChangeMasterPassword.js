// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const ipcRenderer = require('electron').ipcRenderer;

$('#savePassword_btn').click(_ => {

  	//TODO: Validazione campi
	ipcRenderer.sendSync('change_master_password', {
		
		password: $('#password').val(),
		n_password: $('#n_password').val()
	})
})

$('#cancel_btn').click(_ => {

	ipcRenderer.sendSync('cancel', null)
})

ipcRenderer.send('get_password_data_req', null)