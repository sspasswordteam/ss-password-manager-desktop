# ss-password-manager

Applicazione, sviluppata con Electron (http://www.electronjs.org), per salvare in modo cifrato le password, su un file locale.
Al contrario della maggior parte dei Password Manager, questo non utilizza il cloud e non mantiene da nessuna parte una copia della master password.

# Esecuzione

npm install && npm start

# Compilazione

Per generare l'applicazione eseguibile:
npm run dist