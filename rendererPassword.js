// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const ipcRenderer = require('electron').ipcRenderer;

$('#ok_btn').click(_ => {

  	//TODO: Validazione campi
	ipcRenderer.sendSync('master_password', $('#password').val());
})

$('#cancel_btn').click(_ => {

	ipcRenderer.sendSync('cancel', null)
})

$('#password').focus()