// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const ipcRenderer = require('electron').ipcRenderer;

ipcRenderer.on('get_password_data_resp', (sender, data) => {

	$('#id').val(data.id)
	$('#description').val(data.description)
	$('#username').val(data.username)
	$('#password').val(data.password)
})

$('#savePassword_btn').click(_ => {

  	//TODO: Validazione campi
	ipcRenderer.sendSync('edit_password', {
		
		id: $('#id').val(),
		description: $('#description').val(),
		username: $('#username').val(),
		password: $('#password').val()
	})
})

$('#cancel_btn').click(_ => {

	ipcRenderer.sendSync('cancel', null)
})

ipcRenderer.send('get_password_data_req', null)